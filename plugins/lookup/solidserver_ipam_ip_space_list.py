from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
lookup: solidserver_ipam_ip_space_list
short_description: Look up the IP spaces in SOLIDserver IPAM.
description:
  - This module produces a list of all the available IPAM spaces.
options:
  host:
    description: 'SOLIDserver hostname or IP address'
  authentication:
    description: 'Authentication mode (basic|native)'
  username:
    description: 'Authentication username'
  password:
    description: 'Authentication password'
  check_tls:
    description: 'Check SSL certificate (default: True)'
  order:
    description: 'Sort order'
  limit:
    description: 'Results limit'
  offset:
    description: 'Results offset'
'''

EXAMPLES = """
vars:
  spaces: "{{ lookup('solidserver_ipam_ip_space_list', host='192.168.1.1', authentication='basic', username='ipmadmin', password='password', wantlist=True) }}"
tasks:

- name: "use list return option and iterate as a loop"
  debug: msg="{% for space in spaces %}{{ cidr }} {% endfor %}"
# "52.62.0.0/15 52.64.0.0/17 52.64.128.0/17 52.65.0.0/16 52.95.241.0/24 52.95.255.16/28 54.66.0.0/16 "

- name: "Pull IPAM spaces and print the default return style"
  debug: msg="{{ lookup('solidserver_ipam_ip_space_list', host='192.168.1.1', authentication='basic', username='ipmadmin', password='password') }}"
# "52.92.16.0/20,52.216.0.0/15,54.231.0.0/17"
"""

RETURN = """
_raw:
  description: comma-separated list of IPAM spaces
"""


import logging
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import solidserver


class LookupModule(LookupBase):
    def run(self, terms, variables, **kwargs):
        if 'host' not in kwargs:
            raise AnsibleError("Missing SOLIDserver host")
        if 'username' not in kwargs:
            raise AnsibleError("Missing SOLIDserver username")
        if 'password' not in kwargs:
            raise AnsibleError("Missing SOLIDserver password")
        order = None
        if 'order' in kwargs:
            order = kwargs['order']
        limit = None
        if 'limit' in kwargs:
            limit = kwargs['limit']
        offset = None
        if 'offset' in kwargs:
            offset = kwargs['offset']
        provider = {'host': kwargs['host'], 'username': kwargs['username'], 'password': kwargs['password']}
        if 'authentication' in kwargs and kwargs['authentication'] == 'native':
            provider['authentication'] = 'native'
        provider['check_tls'] = 'check_tls' not in kwargs or kwargs['check_tls'] is not False
        sds = solidserver(provider)
        res, code, json = sds.ip_site_list(order, limit, offset)
        if res is False:
            raise AnsibleError("Failed to retrieve IPAM spaces")
        return [site['site_name'] for site in json]
