# community.efficientip Changelog

## [1.0.2] - UNRELEASED

### Fixed

- module import method
- IP address deletion was not idempotent

### Added

- Examples for solidserver_ipam_ip usage

## [1.0.1] - 2021-10-27

### Fixed

- PEP8 code compliance
- Option "update" was not working as intended

### Added

- Example in module solidserver_ipam_ip

## [1.0.0] - 2021-09-06

### Added

- Initial release
